<?php declare(strict_types=1);

namespace App\Allocation\Services;

use App\Allocation\Domain\OrderLine;
use App\Allocation\Domain\Batch;
use App\Allocation\Domain\Product;
use App\Allocation\Adapters\AbstractRepository;
use App\Allocation\Domain\InvalidSku;
use Tightenco\Collect\Support\Collection;

class AllocateService
{
    public function add_batch(string $ref, string $sku, int $qty, ?\DateTimeImmutable $eta, AbstractRepository $repo): void
    {
        # TODO use transaction here to save data to db
        $product = $repo->get($sku);
        if (!$product) {
            $product_domain = new Product($sku, new Collection([]), 1);
            $product = $repo->add($product_domain);
        }
        $batch = new Batch($ref, $sku, $qty, $eta);
        $product->batches->push($batch);
        $repo->save($product);
    }

    public function allocate(string $order_id, string $sku, int $qty, AbstractRepository $repo): string
    {
        # TODO use transaction here to save data to db
        # TODO check isolation_level for DB is "REPEATABLE READ",
        # TODO migrate unit of work to php
        $line = new OrderLine($order_id, $sku, $qty);

        $product = $repo->get($sku);

        if (!$product) {
            throw new InvalidSku("Invalid sku {$line->sku}");
        }

        $batchref = $product->allocate($line);
        return $batchref;
    }
}
