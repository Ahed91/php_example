<?php

function json(array $data, int $status = 200) {
    $response = new \Laminas\Diactoros\Response;
    $response->getBody()->write(json_encode($data));
    $response = $response->withStatus($status);
    return $response;
}

function json_error(array $data, int $status = 400) {
    $response = new \Laminas\Diactoros\Response;
    $response->getBody()->write(json_encode([
        'error' => [
            'status' => $status,
            'name' => $data['name'] ?: "",
            'description' => $data['description'] ?: "",
            'details' => $data['details'] ?: [],
        ],
    ]));
    $response = $response->withStatus($status);
    return $response;
}
