<?php declare(strict_types=1);

namespace App\Allocation\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\Allocation\Adapters\ProductRepository;
use App\Allocation\Adapters\Orm\Orm;
use Tightenco\Collect\Support\Collection as Collection;
use App\Allocation\Domain\OutOfStock;
use App\Allocation\Domain\InvalidSku;
use App\Allocation\Services\AllocateService;

class MainController
{
    protected $repo;
    protected $orm;
    protected $allocate_service;

    public function __construct(Orm $orm, ProductRepository $repo, AllocateService $allocate_service)
    {
        $this->repo = $repo;
        $this->orm = $orm;
        $this->allocate_service = $allocate_service;
    }

    public function add_batch(ServerRequestInterface $request, $params): object
    {
        $ref = $request->getParsedBody()['ref'];
        $sku = $request->getParsedBody()['sku'];
        $qty = intval($request->getParsedBody()['qty']);
        $eta = $request->getParsedBody()['eta'];

        if ($eta !== null) {
            $eta =  new \DateTimeImmutable($eta);
        }

        $batchref = $this->allocate_service->add_batch($ref, $sku, $qty, $eta, $this->repo);

        return json([], 201);
    }

    public function allocate(ServerRequestInterface $request, $params): object
    {
        $order_id = $request->getParsedBody()['order_id'];
        $sku = $request->getParsedBody()['sku'];
        $qty = intval($request->getParsedBody()['qty']);

        try {
            $batchref = $this->allocate_service->allocate($order_id, $sku, $qty, $this->repo);
            return json(['batchref' => $batchref], 201);
        } catch (OutOfStock | InvalidSku $e) {
            return json_error([
                'name' => get_class($e),
                'description' => $e->getMessage(),
                'details' => [],
            ], 400);
        }
    }
}
