<?php declare(strict_types=1);

namespace App\Allocation\Adapters\Orm\Migrations;


class CreateBatchesTable
{
    public function __construct(\App\Allocation\Adapters\Orm\Orm $orm)
    {
        if (!$orm->capsule->getConnection()->getSchemaBuilder()->hasTable('batches')) {
            $orm->capsule->getConnection()->getSchemaBuilder()->create('batches', function ($table) {
                $table->increments('id');
                $table->string('reference')->index();
                $table->string('sku');
                $table->integer('_purchased_quantity');
                $table->datetime('eta')->nullable();
                $table->timestamps();
            });
        }
    }
}
