<?php declare(strict_types=1);

namespace App\Allocation\Adapters\Orm\Migrations;


class CreateAllocationsTable
{
    public function __construct(\App\Allocation\Adapters\Orm\Orm $orm)
    {
        if (!$orm->capsule->getConnection()->getSchemaBuilder()->hasTable('allocations')) {
            $orm->capsule->getConnection()->getSchemaBuilder()->create('allocations', function ($table) {
                $table->increments('id');
                $table->integer('order_line_id')->index();
                $table->integer('batch_id')->index();
                $table->timestamps();
            });
        }
    }
}
