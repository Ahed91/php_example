<?php declare(strict_types=1);

namespace App\Allocation\Adapters\Orm\Migrations;


class CreateProductsTable
{
    public function __construct(\App\Allocation\Adapters\Orm\Orm $orm)
    {
        if (!$orm->capsule->getConnection()->getSchemaBuilder()->hasTable('products')) {
            $orm->capsule->getConnection()->getSchemaBuilder()->create('products', function ($table) {
                $table->increments('id');
                $table->string('sku')->index();
                $table->integer('version_number')->default(0);
                $table->timestamps();
            });
        }
    }
}
