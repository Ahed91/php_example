<?php declare(strict_types=1);

namespace App\Allocation\Adapters\Orm\Migrations;


class CreateOrderLinesTable
{
    public function __construct(\App\Allocation\Adapters\Orm\Orm $orm)
    {
        if (!$orm->capsule->getConnection()->getSchemaBuilder()->hasTable('order_lines')) {
            $orm->capsule->getConnection()->getSchemaBuilder()->create('order_lines', function ($table) {
                $table->increments('id');
                $table->string('order_id')->index();
                $table->string('sku');
                $table->integer('qty');
                $table->timestamps();
            });
        }
    }
}
