<?php

namespace App\Allocation\Adapters\Orm;

use App\Allocation\Adapters\Orm\OrderLine;
use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $with = ['allocations'];
    protected $casts = [
        'id' => 'integer',
        'reference' => 'string',
        'sku' => 'string',
        '_purchased_quantity' => 'integer',
        'eta' => 'datetime',
    ];

    public function allocations()
    {
        return $this->belongsToMany(OrderLine::class, 'allocations');
    }

    public function toDomain()
    {
        $allocations = $this->allocations->map(function ($item) {
            return $item->toDomain();
        })->toArray();
        $allocations = new \Tightenco\Collect\Support\Collection($allocations);

        $eta = null;
        if ($this->eta) {
            $eta = (string) $this->eta;
            $eta = new \DateTimeImmutable($eta);
        }

        return new \App\Allocation\Domain\Batch($this->reference, $this->sku, $this->_purchased_quantity, $eta, $allocations);
    }

    static public function fromDomain(\App\Allocation\Domain\Batch $object)
    {
        $batch = new static;

        $batch->reference = $object->reference;
        $batch->sku = $object->sku;
        $batch->_purchased_quantity = $object->_purchased_quantity;
        $batch->eta = $object->eta;

        return $batch;
    }

    static function getAll($columns = ['*'])
    {
        return static::all($columns)->map(function ($item) {
            return $item->toDomain();
        });
    }
}
