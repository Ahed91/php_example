<?php

namespace App\Allocation\Adapters\Orm;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $with = ['batches'];
    protected $casts = [
        'id' => 'integer',
        'sku' => 'string',
        'version_number' => 'integer',
    ];

    public function batches()
    {
        return $this->hasMany(Batch::class, 'sku', 'sku');
    }

    public function toDomain()
    {
        $batches = $this->batches->map(function ($item) {
            return $item->toDomain();
        })->toArray();
        $batches = new \Tightenco\Collect\Support\Collection($batches);

        return new \App\Allocation\Domain\Product($this->sku, $batches, $this->version_number);
    }

    static public function fromDomain(\App\Allocation\Domain\Product $object)
    {
        $product = new static;

        $product->sku = $object->sku;
        $product->version_number = $object->version_number;

        $batches = $object->batches->map(function ($item) {
            return Batch::fromDomain($item);
        });

        return [ $product, $batches ];
    }

    static function getAll($columns = ['*'])
    {
        return static::all($columns)->map(function ($item) {
            return $item->toDomain();
        });
    }
}

