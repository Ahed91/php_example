<?php


namespace App\Allocation\Adapters\Orm;


use App\Allocation\Adapters\Orm\Migrations\CreateAllocationsTable;
use App\Allocation\Adapters\Orm\Migrations\CreateBatchesTable;
use App\Allocation\Adapters\Orm\Migrations\CreateOrderLinesTable;
use App\Allocation\Adapters\Orm\Migrations\CreateProductsTable;

class MigrationManager
{
    private $orm;

    public function __construct($orm)
    {
        $this->orm = $orm;
    }

    public function execute()
    {
        $classes = [
            CreateOrderLinesTable::class,
            CreateBatchesTable::class,
            CreateAllocationsTable::class,
            CreateProductsTable::class,
        ];

        foreach ($classes as $class) {
            new $class($this->orm);
        }
    }
}
