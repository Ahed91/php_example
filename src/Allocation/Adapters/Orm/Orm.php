<?php


namespace App\Allocation\Adapters\Orm;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


class Orm
{
    public $capsule;

    public function __construct(array $connection, array $params = [])
    {
        $this->capsule = new Capsule;

        $this->capsule->addConnection($connection);

        $this->capsule->bootEloquent();

        Model::setConnectionResolver($this->capsule->getDatabaseManager());

        if (isset($params['do_migration'])) {
            $migration_manager = new MigrationManager($this);
            $migration_manager->execute();
        }

        // set TRANSACTION ISOLATION LEVEL
        /* $pdo = $this->getConnection->getPdo(); */
        /* $pdo->exec('SET TRANSACTION ISOLATION LEVEL READ COMMITTED'); */
    }

    public function getConnection()
    {
        return $this->capsule->getConnection();
    }

    public function select(string $query, array $params = [], string $result_type = 'array')
    {
        $result = $this->getConnection()->select($query, $params);
        if ($result_type === 'array') {
            $result = (new Collection($result))->map(function ($item) {
                return array_values((array)$item);
            })->toArray();
        }

        return $result;
    }
}
