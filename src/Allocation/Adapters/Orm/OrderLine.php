<?php

namespace App\Allocation\Adapters\Orm;

use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model
{
    protected $casts = [
        'id' => 'integer',
        'sku' => 'string',
        'qty' => 'integer',
        'order_id' => 'string',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function toDomain()
    {
        return new \App\Allocation\Domain\OrderLine($this->order_id, $this->sku, $this->qty);
    }

    static public function fromDomain(\App\Allocation\Domain\OrderLine $object)
    {
        $order_line = new static;

        $order_line->sku = $object->sku;
        $order_line->qty = $object->qty;
        $order_line->order_id = $object->order_id;

        return $order_line;
    }

    static function getAll($columns = ['*'])
    {
        return static::all($columns)->map(function ($item) {
            return $item->toDomain();
        });
    }

}
