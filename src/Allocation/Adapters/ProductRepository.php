<?php


namespace App\Allocation\Adapters;


use App\Allocation\Adapters\AbstractRepository;
use App\Allocation\Adapters\Orm\Product;
use App\Allocation\Adapters\Orm\Orm;
use Tightenco\Collect\Support\Collection;

class ProductRepository extends AbstractRepository
{
    public $orm;

    public function __construct(Orm $orm)
    {
        $this->orm = $orm;
    }

    function add(\App\Allocation\Domain\Product $object)
    {
        list($product, $batches) = Product::fromDomain($object);
        $product->save();
        $product->batches()->saveMany($batches);
        return $product->toDomain();
    }

    function get(string $sku)
    {
        $product = Product::where('sku', $sku)->first();
        if ($product) {
            return $product->toDomain();
        }
        return $Product;
    }

    function save(\App\Allocation\Domain\Product $object)
    {
        list($product, $batches) = Product::fromDomain($object);
        $product->save();
        $product->batches()->saveMany($batches);
        return $product->toDomain();
    }
}
