<?php

namespace App\Allocation\Adapters;

use Tightenco\Collect\Support\Collection;

class FakeRepository extends AbstractRepository
{
    public $_products;

    public function __construct(Collection $products)
    {
        $this->_products = $products;
    }

    function add(\App\Allocation\Domain\Product $object)
    {
        $this->_products->push($object);
        return $object;
    }

    function get(string $sku)
    {
        $index = $this->_products->search(function ($item) use ($sku) {
            return $sku === $item->sku;
        });
        if ($index !== false) {
            return $this->_products[$index];
        } else {
            return null;
        }
    }

    function save(\App\Allocation\Domain\Product $object)
    {
        // remove old
        $index = $this->_products->search(function ($item) use ($object) {
            return $item->sku === $object->sku;
        });
        unset($this->_products[$index]);

        // insert new
        $this->_products->push($object);
        return $object;
    }
}
