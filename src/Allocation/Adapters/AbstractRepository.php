<?php
declare(strict_types=1);

namespace App\Allocation\Adapters;

use App\Allocation\Domain\Product;
use Exception;

class AbstractRepository
{
    function add(Product $product)
    {
        throw new Exception('Not implemented');
    }

    function get(string $sku)
    {
        throw new Exception('Not implemented');
    }
}
