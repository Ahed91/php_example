<?php

require dirname(__file__) . '/../../vendor/autoload.php';
require dirname(__file__) . '/helpers.php';

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Allocation\Adapters\ProductRepository;
use App\Allocation\Adapters\Orm\Orm;

// prepare dependencies
$container = new League\Container\Container;

// register the reflection container as a delegate to enable auto wiring
$reflection_container = new League\Container\ReflectionContainer;
$reflection_container->cacheResolutions();
$container->delegate($reflection_container);

$connection = [
    // testing
    'driver' => 'sqlite',
    'database' => dirname(__file__) . '/storage/test.db',

     /* 'driver' => 'mysql', */
     'host' => 'localhost',
     /* 'database' => 'database', */
     'username' => '',
     'password' => '',
     'charset' => 'utf8mb4',
     'collation' => 'utf8mb4_unicode_ci',
     'prefix' => '',
];
$orm = new Orm($connection);
$container->add(Orm::class, $orm)->setShared();

$repo = new ProductRepository($orm);
$container->add(ProductRepository::class, $repo)->setShared();

// prepare router
$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$responseFactory = new \Laminas\Diactoros\ResponseFactory();

$strategy = (new League\Route\Strategy\JsonStrategy($responseFactory))->setContainer($container);;
$router   = (new League\Route\Router)->setStrategy($strategy);
