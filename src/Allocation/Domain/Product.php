<?php

namespace App\Allocation\Domain;

use App\Allocation\Domain\OrderLine;
use Tightenco\Collect\Support\Collection;

final class Product
{
    public $sku;
    public $batches;
    public $version_number;

    public function __construct(string $sku, Collection $batches, int $version_number = 1)
    {
        $this->sku = $sku;
        $this->batches = $batches ?: new Collection();
        $this->version_number = $version_number;
    }

    public function allocate(OrderLine $line): string
    {
        $batch = $this->batches
            ->filter(function (Batch $batch) use ($line) {
                return $batch->can_allocate($line);
            })
            ->sortBy(function ($batch) {
                return $batch->eta ? $batch->eta->getTimestamp() : 0;
            })
            ->first();

        if (!$batch) {
            throw new OutOfStock("Out of stock for order {$line->order_id}");
        }

        $batch->allocate($line);

        $this->version_number += 1;

        return $batch->reference;
    }
}
