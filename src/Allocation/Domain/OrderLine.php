<?php


namespace App\Allocation\Domain;


final class OrderLine
{
    public $order_id;
    public $sku;
    public $qty;

    public function __construct(string $order_id, string $sku, int $qty)
    {
        $this->order_id = $order_id;
        $this->sku = $sku;
        $this->qty = $qty;
    }

}
