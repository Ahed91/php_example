<?php

namespace App\Allocation\Domain;

use App\Allocation\Domain\OrderLine;
use DateTimeImmutable;
use Tightenco\Collect\Support\Collection;

final class Batch
{
    public $reference;
    public $sku;
    public $_purchased_quantity;
    public $eta;

    public function __construct(string $reference, string $sku, int $qty, DateTimeImmutable $eta = null, Collection $allocations = null)
    {
        $this->reference = $reference;
        $this->sku = $sku;
        $this->eta = $eta;

        $this->_purchased_quantity = $qty;
        $this->_allocations = $allocations ?: new Collection();
    }

    /**
     * @param OrderLine $line
     * @return void
     */
    public function allocate(OrderLine $line): void
    {
        if ($this->can_allocate($line)) {
            $this->_allocations = $this->_allocations->push($line)->unique("order_id");
        }
    }

    public function deallocate(OrderLine $line): void
    {
        $index = $this->_allocations->search(function ($item) use ($line) {
            return $line->order_id === $item->order_id;
        });
        if ($index !== false) {
            $this->_allocations->forget($index);
        }
    }

    public function get_allocated_quantity(): int
    {
        return $this->_allocations->map(function ($line) {
            return $line->qty;
        })->sum();
    }


    public function get_available_quantity(): int
    {
        return $this->_purchased_quantity - $this->get_allocated_quantity();
    }

    public function can_allocate(OrderLine $line): bool
    {
        return ($this->sku == $line->sku) && ($this->get_available_quantity() >= $line->qty);
    }
}
