<?php declare(strict_types=1);

require './../bootstrap.php';

// map a route
$router->map('POST', '/allocate', [App\Allocation\Controllers\MainController::class, 'allocate']);
$router->map('POST', '/add_batch', [App\Allocation\Controllers\MainController::class, 'add_batch']);

$response = $router->dispatch($request);

// send the response to the browser
(new Laminas\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);
