<?php declare(strict_types=1);

use App\Allocation\Domain\OrderLine;
use App\Allocation\Domain\InvalidSku;
use PHPUnit\Framework\TestCase;
use App\Allocation\Adapters\FakeRepository;
use App\Allocation\Domain\Batch;
use Tightenco\Collect\Support\Collection;
use App\Allocation\Services\AllocateService;

final class AllocateServiceTest extends TestCase
{
    protected $allocate_servicelater;

    protected function setUp(): void
    {
        $this->allocate_service = new AllocateService();
    }

    public function test_add_batch_for_new_product(): void
    {
        $repo = new FakeRepository(new Collection([]));

        $result = $this->allocate_service->add_batch("b1", "CRUNCHY-ARMCHAIR", 100, null, $repo);

        $this->assertNotEquals($repo->get("CRUNCHY-ARMCHAIR"), null);
    }

    /* public function test_add_batch_for_existing_product(): void */
    /* { */
    /*     $repo = new FakeRepository(new Collection([])); */

    /*     $result = $this->allocate_service->add_batch("b1", "GARISH-RUG", 100, null, $repo); */
    /*     $result = $this->allocate_service->add_batch("b2", "GARISH-RUG", 99, null, $repo); */

    /*     $reference = $repo->get("GARISH-RUG")->batches->filter(function ($batch) { */
    /*         return $batch->reference === "b2"; */
    /*     })->map(function ($batch) { */
    /*         return $batch->reference; */
    /*     })->first(); */

    /*     $this->assertEquals("b2", $reference); */
    /* } */

/*     public function test_allocate_returns_allocation(): void */
/*     { */
/*         $repo = new FakeRepository(new Collection([])); */
/*         $result = $this->allocate_service->add_batch("batch1", "COMPLICATED-LAMP", 100, null, $repo); */

/*         $result = $this->allocate_service->allocate("o1", "COMPLICATED-LAMP", 10, $repo); */

/*         $this->assertSame("batch1", $result); */
/*     } */

/*     public function test_allocate_errors_for_invalid_sku(): void */
/*     { */
/*         $repo = new FakeRepository(new Collection([])); */
/*         $result = $this->allocate_service->add_batch("b1", "AREALSKU", 100, null, $repo); */

/*         $this->expectException(InvalidSku::class); */
/*         $this->expectExceptionMessage("Invalid sku NONEXISTENTSKU"); */
/*         $this->allocate_service->allocate("o1", "NONEXISTENTSKU", 10, $repo); */
/*     } */
}
