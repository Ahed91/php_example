<?php declare(strict_types=1);

use App\Allocation\Domain\OrderLine;
use App\Allocation\Domain\OutOfStock;
use PHPUnit\Framework\TestCase;


use App\Allocation\Domain\Batch;
use App\Allocation\Domain\Product;
use Tightenco\Collect\Support\Collection;


final class ProductTest extends TestCase
{
    protected $today;
    protected $tomorrow;
    protected $later;

    protected function setUp(): void
    {
        $this->today = new DateTimeImmutable();
        $this->tomorrow = $this->today->add(new DateInterval('P1D'));
        $this->later = $this->today->add(new DateInterval('P10D'));
    }

    protected function make_batch_and_line(string $sku, int $batch_qty, int $line_aty): array
    {
        return [
            new Batch("batch-001", $sku, $batch_qty, $this->today),
            new OrderLine('order-123', $sku, $line_aty)
        ];
    }

    public function test_prefers_warehouse_batches_to_shipments(): void
    {
        $in_stock_batch = new Batch("in-stock-batch", "RETRO-CLOCK", 100, null);
        $shipment_batch = new Batch("shipment-batch", "RETRO-CLOCK", 100, null);
        $product = new Product("RETRO-CLOCK", new Collection([$in_stock_batch, $shipment_batch]));
        $line = new OrderLine("oref", "RETRO-CLOCK", 10);

        $product->allocate($line);

        $this->assertSame($in_stock_batch->get_available_quantity(), 90);
        $this->assertSame($shipment_batch->get_available_quantity(), 100);

    }

    public function test_prefers_earlier_batches(): void
    {
        $earliest = new Batch("speedy-batch", "MINIMALIST-SPOON", 100, $this->today);
        $medium = new Batch("normal-batch", "MINIMALIST-SPOON", 100, $this->tomorrow);
        $latest = new Batch("slow-batch", "MINIMALIST-SPOON", 100, $this->later);
        $product = new Product("MINIMALIST-SPOON", new Collection([$medium, $earliest, $latest]));
        $line = new OrderLine("order1", "MINIMALIST-SPOON", 10);

        $product->allocate($line);

        $this->assertSame($earliest->get_available_quantity(), 90);
        $this->assertSame($medium->get_available_quantity(), 100);
        $this->assertSame($latest->get_available_quantity(), 100);
    }

    public function test_returns_allocated_batch_ref(): void
    {
        $in_stock_batch = new Batch("in-stock-batch-ref", "HIGHBROW-POSTER", 100, null);
        $shipment_batch = new Batch("shipment-batch-ref", "HIGHBROW-POSTER", 100, $this->tomorrow);
        $line = new OrderLine("order1", "HIGHBROW-POSTER", 10);

        $product = new Product("HIGHBROW-POSTER", new Collection([$in_stock_batch, $shipment_batch]));
        $allocation = $product->allocate($line);

        $this->assertSame($in_stock_batch->reference, $allocation);
    }

    public function test_raises_out_of_stock_exception_if_cannot_allocate(): void
    {
        $batch = new Batch('batch1', 'SMALL-FORK', 10, $this->today);
        $line1 = new OrderLine('order1', 'SMALL-FORK', 10);
        $line2 = new OrderLine('order2', 'SMALL-FORK', 1);

        $product = new Product("SMALL-FORK", new Collection([$batch]));
        $product->allocate($line1);


        $this->expectException(OutOfStock::class);
        $this->expectExceptionMessage("order2");
        $product->allocate($line2);
    }

    public function test_increments_version_number(): void
    {
        $line = new OrderLine('oref', "SCANDI-PEN", 10);
        $batch = new Batch('b1', "SCANDI-PEN", 100, null);
        $product = new Product("SCANDI-PEN", new Collection([$batch]));
        $product->version_number = 7;

        $product->allocate($line);

        $this->assertSame(8, $product->version_number);
    }
}
