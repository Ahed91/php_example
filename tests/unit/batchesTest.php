<?php declare(strict_types=1);

use App\Allocation\Domain\OrderLine;
use PHPUnit\Framework\TestCase;


use App\Allocation\Domain\Batch;

final class batchesTest extends TestCase
{
    protected $today;
    protected $tomorrow;
    protected $later;

    protected function setUp(): void
    {
        $this->today = new DateTimeImmutable();
        $this->tomorrow = $this->today->add(new DateInterval('P1D'));
        $this->later = $this->today->add(new DateInterval('P10D'));
    }


    protected function make_batch_and_line(string $sku, int $batch_qty, int $line_aty): array
    {
        return [
            new Batch("batch-001", $sku, $batch_qty, $this->today),
            new OrderLine('order-123', $sku, $line_aty)
        ];
    }

    public function test_allocating_to_a_batch_reduces_the_available_quantity(): void
    {
        $batch = new Batch("batch-001", "SMALL-TABLE", 20, $this->today);
        $line = new OrderLine('order-ref', "SMALL-TABLE", 2);
        $batch->allocate($line);
        $this->assertSame(18, $batch->get_available_quantity());

    }

    public function test_can_allocate_if_available_greater_than_required(): void
    {
        list($large_batch, $small_line) = $this->make_batch_and_line("ELEGANT-LAMP", 20, 2);
        $this->assertTrue($large_batch->can_allocate($small_line));
    }

    public function test_cannot_allocate_if_available_smaller_than_required(): void
    {
        list($small_batch, $large_line) = $this->make_batch_and_line("ELEGANT-LAMP", 2, 20);
        $this->assertFalse($small_batch->can_allocate($large_line));
    }

    public function test_can_allocate_if_available_equal_to_required(): void
    {
        list($batch, $line) = $this->make_batch_and_line("ELEGANT-LAMP", 2, 2);
        $this->assertTrue($batch->can_allocate($line));
    }

    public function test_cannot_allocate_if_skus_do_not_match(): void
    {
        $batch = new Batch("batch-001", "UNCOMFORTABLE-CHAIR", 100, null);
        $different_sku_line = new OrderLine("order-123", "EXPENSIVE-TOASTER", 10);
        $this->assertFalse($batch->can_allocate($different_sku_line));
    }


    public function test_allocation_is_idempotent(): void
    {
        list($batch, $line) = $this->make_batch_and_line("ANGULAR-DESK", 20, 2);
        $batch->allocate($line);
        $batch->allocate($line);
        $this->assertSame(18, $batch->get_available_quantity());
    }

    public function test_deallocate(): void
    {
        list($batch, $line) = $this->make_batch_and_line("EXPENSIVE-FOOTSTOOL", 20, 2);
        $batch->allocate($line);
        $batch->deallocate($line);
        $this->assertSame(20, $batch->get_available_quantity());
    }

    public function test_can_only_deallocate_allocated_lines(): void
    {
        list($batch, $unallocated_line) = $this->make_batch_and_line("DECORATIVE-TRINKET", 20, 2);
        $batch->deallocate($unallocated_line);
        $this->assertSame(20, $batch->get_available_quantity());
    }

}

