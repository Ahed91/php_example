<?php declare(strict_types=1);

use App\Allocation\Adapters\Orm\OrderLine;
use App\Allocation\Adapters\Orm\Orm;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Illuminate\Database\Eloquent\Model;


final class OrmTest extends TestCase
{
    private $orm;

    protected function setUp(): void
    {
        $connection = [
            // testing
            'driver' => 'sqlite',
            'database' => ':memory:',
            'host' => 'localhost',
            'username' => '',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ];

        $this->orm = new Orm($connection, ['do_migration' => true]);
    }

    public function test_orderline_mapper_can_load_lines(): void
    {
        $this->orm->getConnection()->unprepared('
            INSERT INTO order_lines (order_id, sku, qty) VALUES
            ("order1", "RED-CHAIR", 12),
            ("order1", "RED-TABLE", 13),
            ("order2", "BLUE-LIPSTICK", 14)
        ');

        $expected = new Collection([
            new \App\Allocation\Domain\OrderLine("order1", "RED-CHAIR", 12),
            new \App\Allocation\Domain\OrderLine("order1", "RED-TABLE", 13),
            new \App\Allocation\Domain\OrderLine("order2", "BLUE-LIPSTICK", 14),
        ]);

        $actual = OrderLine::getAll();

        $this->assertEquals($expected, $actual);
    }

    public function test_orderline_mapper_can_save_lines(): void
    {
        $new_line = new \App\Allocation\Domain\OrderLine("order1", "DECORATIVE-WIDGET", 12);
        OrderLine::fromDomain($new_line)->save();

        $actual = $this->orm->select('SELECT order_id, sku, qty FROM order_lines');

        $this->assertEquals([["order1", "DECORATIVE-WIDGET", 12]], $actual);
    }
}
