<?php declare(strict_types=1);

use App\Allocation\Adapters\FakeRepository as Repository;
use PHPUnit\Framework\TestCase;
use Tightenco\Collect\Support\Collection;


final class FakeRepositoryTest extends TestCase
{
    protected function setUp(): void
    {
    }

    public function insert_order_line(): string
    {
        // @note implement
        return "";
    }

    public function insert_batch($batch_id): int
    {
        // @note implement
        return 1;
    }


    public function insert_allocation($order_line_id, $batch_id): void
    {
        // @note implement
    }

    public function test_repository_can_save_a_batch(): void
    {
        $batch = new \App\Allocation\Domain\Batch("batch1", "RUSTY-SOAPDISH", 100, null);
        $product = new \App\Allocation\Domain\Product("RUSTY-SOAPDISH", new Collection([]), 1);
        $product->batches->push($batch);

        $repo = new Repository(new Collection([]));
        $repo->add($product);

        $actual = $repo->get("RUSTY-SOAPDISH")->batches->map(function ($item) {
            $item = (array) $item;
            return array_values($item);
        })->toArray();

        $this->assertEquals([["batch1", "RUSTY-SOAPDISH", 100, null, new Collection([])]], $actual);
    }

    /* public function test_repository_can_retrieve_a_batch_with_allocations(): void */
    /* { */
    /*     // @note implement */
    /* } */
}
