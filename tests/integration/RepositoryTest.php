<?php declare(strict_types=1);

use App\Allocation\Adapters\ProductRepository;
use App\Allocation\Adapters\Orm\OrderLine;
use App\Allocation\Adapters\Orm\Orm;
use PHPUnit\Framework\TestCase;
use Illuminate\Database\Eloquent\Model;
use Tightenco\Collect\Support\Collection;


final class RepositoryTest extends TestCase
{
    private $orm;

    protected function setUp(): void
    {
        $connection = [
            // testing
            'driver' => 'sqlite',
            'database' => ':memory:',
            'host' => 'localhost',
            'username' => '',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ];

        $this->orm = new Orm($connection, ['do_migration' => true]);
    }

    public function insert_order_line(): string
    {
        $this->orm->getConnection()->insert('
            INSERT INTO order_lines (order_id, sku, qty)
             VALUES ("order1", "GENERIC-SOFA", 12)
        ');
        $order_line_id = $this->orm->select('
            SELECT id FROM order_lines WHERE order_id=:order_id AND sku=:sku
        ', ['order_id' => 'order1', 'sku' => 'GENERIC-SOFA'])
        [0][0];

        return $order_line_id;
    }

    public function insert_batch($batch_id): int
    {
        $this->orm->getConnection()->insert('
            INSERT INTO batches (reference, sku, _purchased_quantity, eta)
              VALUES (:batch_id, "GENERIC-SOFA", 100, null)
        ', ['batch_id' => $batch_id]);

        $batch_id = $this->orm->select('
            SELECT id FROM batches WHERE reference=:batch_id AND sku="GENERIC-SOFA"
        ', ['batch_id' => $batch_id])
        [0][0];
        $batch_id = (int) $batch_id;
        return $batch_id;
    }

    public function insert_allocation($order_line_id, $batch_id): void
    {
        $this->orm->getConnection()->insert('
            INSERT INTO allocations (order_line_id, batch_id)
              VALUES (:order_line_id, :batch_id)
        ', ['order_line_id' => $order_line_id, 'batch_id' => $batch_id]);
    }

    public function insert_product(): int
    {
        $this->orm->getConnection()->insert('
            INSERT INTO products (sku, version_number)
              VALUES ("GENERIC-SOFA", 1)
        ');

        $product_id = $this->orm->select('
            SELECT id FROM products WHERE sku="GENERIC-SOFA"
        ')
        [0][0];
        $product_id = (int) $product_id;
        return $product_id;
    }

    public function test_repository_can_save_a_batch(): void
    {
        $batch = new \App\Allocation\Domain\Batch("batch1", "RUSTY-SOAPDISH", 100, null);
        $product = new \App\Allocation\Domain\Product("RUSTY-SOAPDISH", new Collection([]), 1);
        $product->batches->push($batch);

        $repo = new ProductRepository($this->orm);
        $repo->add($product);

        $actual = $this->orm->select('
            SELECT reference, sku, _purchased_quantity, eta FROM "batches"
            ');
        $this->assertEquals([["batch1", "RUSTY-SOAPDISH", 100, null]], $actual);
    }

    public function test_repository_can_retrieve_a_batch_with_allocations(): void
    {
        $order_line_id = $this->insert_order_line();
        $batch1_id = $this->insert_batch("batch1");
        $this->insert_batch("batch2");
        $this->insert_allocation($order_line_id, $batch1_id);
        $this->insert_product();

        $repo = new ProductRepository($this->orm);
        $retrieved = $repo->get("GENERIC-SOFA");
        $retrieved = $retrieved->batches->filter(function($item) {
            return $item->sku === "GENERIC-SOFA";
        })->first();

        $expected = new \App\Allocation\Domain\Batch("batch1", "GENERIC-SOFA", 100);;
        $this->assertEquals($expected->reference, $retrieved->reference);
        $this->assertEquals($expected->sku, $retrieved->sku);
        $this->assertEquals($expected->_purchased_quantity, $retrieved->_purchased_quantity);
        $this->assertEquals($retrieved->_allocations, new Collection([
            new \App\Allocation\Domain\OrderLine("order1", "GENERIC-SOFA", 12),
        ]));
    }
}
