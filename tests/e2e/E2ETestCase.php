<?php declare(strict_types=1);

use App\Allocation\Adapters\Orm\Orm;
use PHPUnit\Framework\TestCase;

class E2ETestCase extends TestCase
{
    protected static $orm;
    protected $batches_added = [];
    protected $skus_added = [];

    public static function setUpBeforeClass(): void
    {
        $connection = [
            'driver' => 'sqlite',
            'database' => dirname(__file__) . '/../../src/Allocation/storage/test.db',
            'host' => 'localhost',
            'username' => '',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ];

        self::$orm = new Orm($connection, ['do_migration' => true]);
    }

    public static function tearDownAfterClass(): void
    {
        self::$orm = null;
    }
}
