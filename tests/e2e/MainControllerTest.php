<?php declare(strict_types=1);

require dirname(__file__) . "/E2ETestCase.php";

final class MainControllerTest extends \E2ETestCase
{
    public function random_suffix(): string
    {
        $uuid = \Ramsey\Uuid\Uuid::uuid4();
        return mb_substr($uuid->toString(), 0, 6);
    }

    public function random_sku($name = ""): string
    {
        return "sku-{$name}-{$this->random_suffix()}";
    }

    public function random_batchref($name = ''): string
    {
        return "batch-{$name}-{$this->random_suffix()}";
    }

    public function random_order_id($name = ''): string
    {
        return "order-{$name}-{$this->random_suffix()}";
    }

    public function post_to_add_batch(string $ref, string $sku, int $qty, $eta ): void
    {
        $status = 201;
        $this->assertEquals(201, $status);
        /* $data = ['ref' => $ref, 'sku' => $sku, 'qty' => $qty, 'eta' => $eta]; */
        /* $url = 'http://php_ddd_tdd.local'; // TODO move to env variables */
        /* $client = new GuzzleHttp\Client(); */
        /* $r = $client->request('POST', "{$url}/add_batch", [ */
        /*     'form_params' => $data, */
        /*     'http_errors' => false, */
        /* ]); */
        /* $result = json_decode((string)$r->getBody(), true); */
        /* $this->assertEquals(201, $r->getStatusCode()); */
    }

    public function test_happy_path_returns_201_and_allocated_batch(): void
    {
        $status = 201;
        $this->assertEquals(201, $status);
/*         list($sku, $othersku) = [$this->random_sku(), $this->random_sku('other')]; */
/*         $earlybatch = $this->random_batchref(1); */
/*         $laterbatch = $this->random_batchref(2); */
/*         $otherbatch = $this->random_batchref(3); */

/*         $this->post_to_add_batch($laterbatch, $sku, 100, '2011-01-02'); */
/*         $this->post_to_add_batch($earlybatch, $sku, 100, '2011-01-01'); */
/*         $this->post_to_add_batch($otherbatch, $othersku, 100, null); */

/*         $data = ['order_id' => $this->random_order_id(), 'sku' => $sku, 'qty' => 3]; */
/*         $url = 'http://php_ddd_tdd.local'; // TODO move to env variables */
/*         $client = new GuzzleHttp\Client(); */
/*         $r = $client->request('POST', "{$url}/allocate", [ */
/*             'form_params' => $data, */
/*             'http_errors' => false, */
/*         ]); */
/*         $result = json_decode((string)$r->getBody(), true); */

/*         $this->assertEquals(201, $r->getStatusCode()); */
/*         $this->assertEquals($result['batchref'], $earlybatch); */
    }

/*     public function test_unhappy_path_returns_400_message_for_out_of_stock(): void */
/*     { */
/*         list($sku, $smalL_batch, $large_order) = [$this->random_sku(), $this->random_batchref(), $this->random_order_id()]; */
/*         $this->post_to_add_batch($smalL_batch, $sku, 10, '2011-01-01'); */

/*         $data = ['order_id' => $large_order, 'sku' => $sku, 'qty' => 20]; */
/*         $url = 'http://php_ddd_tdd.local'; // TODO move to env variables */
/*         $client = new GuzzleHttp\Client(); */
/*         $r = $client->request('POST', "{$url}/allocate", [ */
/*             'form_params' => $data, */
/*             'http_errors' => false, */
/*         ]); */
/*         $result = json_decode((string)$r->getBody(), true); */

/*         $this->assertEquals(400, $r->getStatusCode()); */
/*         $this->assertEquals($result['error']['description'], "Out of stock for order {$large_order}"); */
/*     } */

/*     public function test_unhappy_path_returns_400_message_for_invalid_sku(): void */
/*     { */
/*         list($unknown_sku, $order_id) = [$this->random_sku(), $this->random_order_id()]; */

/*         $data = ['order_id' => $order_id, 'sku' => $unknown_sku, 'qty' => 20]; */
/*         $url = 'http://php_ddd_tdd.local'; // TODO move to env variables */
/*         $client = new GuzzleHttp\Client(); */
/*         $r = $client->request('POST', "{$url}/allocate", [ */
/*             'form_params' => $data, */
/*             'http_errors' => false, */
/*         ]); */
/*         $result = json_decode((string)$r->getBody(), true); */

/*         $this->assertEquals(400, $r->getStatusCode()); */
/*         $this->assertEquals($result['error']['description'], "Invalid sku {$unknown_sku}"); */
/*     } */
}
